class Arbol{
    constructor() {
        this.nodoPadre = "";
        this.nodos = [];
       // this.busquedaElemento;
       // this.caminoNodo = " ";
       // this.sumaDeCaminos = 0;
    }



    NodoPadre(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre)
        this.nodos.push(nodo);
        return nodo;
    }



    Nodo(padre, nivel, posicion, valor, nombre){
        var nodo = new Nodo(padre, nivel, posicion, valor, nombre)
        this.nodos.push(nodo);
        return nodo;
    }


    buscarNodosPadre(padre){
        let nodosEncontrados = [];
        let nodo = null;
        for(let x = 0; x < this.nodos; x++){
            if(this.nodos[x].padre == padre){
                nodosEncontrados.push(this.nodos[x]);

            }
            return nodosEncontrados;

        }
    }

    agregarNodo(valor, nombre){
        let nodo = new Nodo(null, null, null, valor, nombre);

        if(this.nodos.length == 1){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1 ;
            if(nodo.valor < this.nodoPadre.valor) {
                // izquierda
                nodo.posicion = "hIzq";

            } else{
                // derecha
                nodo.posicion = "hDer";


            }
            this.nodos.push(nodo);

        }




    }
    agregarNodo2(valor, nombre){
        let nodo = new Nodo(null, null, null, valor, nombre);

        if(this.nodos.length == 2){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1 ;
            if(nodo.valor < this.nodoPadre.valor) {
                // izquierda
                nodo.posicion = "hIzq";

            } else{
                // derecha
                nodo.posicion = "hDer";


            }
            this.nodos.push(nodo);

        }




    }
    agregarNodo3(valor, nombre){
        let nodo = new Nodo(null, null, null, valor, nombre);

        if(this.nodos.length == 3){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1 ;
            if(nodo.valor < this.nodoPadre.valor) {
                // izquierda
                nodo.posicion = "hIzq";

            } else{
                // derecha
                nodo.posicion = "hDer";


            }
            this.nodos.push(nodo);

        }




    }
    agregarNodo4(valor, nombre){
        let nodo = new Nodo(null, null, null, valor, nombre);

        if(this.nodos.length == 4){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 1 ;
            if(nodo.valor < this.nodoPadre.valor) {
                // izquierda
                nodo.posicion = "hIzq";

            } else{
                // derecha
                nodo.posicion = "hDer";


            }
            this.nodos.push(nodo);

        }




    }
    agregarNodo5(valor, nombre){
        let nodo = new Nodo(null, null, null, valor, nombre);

        if(this.nodos.length == 5){
            nodo.padre = this.nodoPadre;
            nodo.nivel = this.nodoPadre.nivel + 2 ;
            if(nodo.valor < this.nodoPadre.valor) {
                // izquierda
                nodo.posicion = "hIzq";

            } else{
                // derecha
                nodo.posicion = "hDer";


            }
            this.nodos.push(nodo);

        }




    }
}